﻿
using UnityEngine;
using Photon.Pun;


[RequireComponent(typeof(PhotonTransformView))]
public class PunUserNetControl : MonoBehaviourPunCallbacks  ,  IPunInstantiateMagicCallback
{
    [Tooltip("The local player instance.  Use  this to know if  the local  player is repersented  in the Scene")]
    public static GameObject LocalPlayerInstance;

   public void OnPhotonInstantiate(PhotonMessageInfo info)
    {
        if (photonView.IsMine)
        {
            LocalPlayerInstance = gameObject;
        }
        else
        {
            GetComponentInChildren<Camera>().enabled = false;
            GetComponent<FirstPersonMovement>().enabled = false;
        }
    }
}
