﻿
using UnityEngine;
using Photon.Pun;
using Photon.Pun.UtilityScripts;


public class PunNetworkManager : ConnectAndJoinRandom
{
    public static PunNetworkManager singleton;

    [Header("Spawn  Info")]
    [Tooltip("The prefab to use for representing  the player")]

    public GameObject GamePlayerPrefab;
    public GameObject GunPrefab;

    private void Awake()
    {
        singleton = this;
    }
    public override void OnCreatedRoom()
    {
        PhotonNetwork.Instantiate(GunPrefab.name,
            new Vector3(0f, 5f, 0f), Quaternion.identity, 0);
    }

    public override void OnJoinedRoom()
    {
        base.OnJoinedRoom();
        Camera.main.gameObject.SetActive(false);
        if(PunUserNetControl.LocalPlayerInstance ==  null)
        {
            Debug.Log("we are Instantiating LocalPlayer form " + SceneManagerHelper.ActiveSceneName);
            PunNetworkManager.singleton.SpawnPlayer();
        }
        else
        {
            Debug.Log("Ignoring scene load for " + SceneManagerHelper.ActiveSceneName);

        }
    }
    public void  SpawnPlayer()
    {
        PhotonNetwork.Instantiate(GamePlayerPrefab.name,
            new Vector3(0f, 5f, 0f), Quaternion.identity, 0);
 
    }

}
