﻿using UnityEngine;
using Photon.Pun;

public class GunNetControl : MonoBehaviourPunCallbacks, IPunInstantiateMagicCallback
{
    public static GameObject LocalGunInstance;
    public void OnPhotonInstantiate(PhotonMessageInfo info)
    {
        if (photonView.IsMine)
        {
            LocalGunInstance = gameObject;
        }
       
    }
}
